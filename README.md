Quickstart:


0. Import GPG key maually. This is not automated. 
1. Generate new SSH key and add (temporarily) to gitlab
2. Run

    chezmoi init --apply git@gitlab.com:charlesbaynham/dotfiles.git
3. Delete ssh key and log out / in. 

